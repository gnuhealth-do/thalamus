#! /bin/bash

# SPDX-FileCopyrightText: 2024 Gerald Wiese
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Usage:
# 1. bash lint.sh
# 2. bash lint.sh |grep -v pyc |grep -v __init__ |grep -v setup

# Abort on non catched errors
set -e

# Following uses PyPi package flake8, run "pipx install flake8" to get it
echo "Running pycodestyle linting"
pycodestyle .  || exit_status=$?
printf "\n\n\nRunning pyflakes linting\n"
pyflakes . || exit_status=$?

# Following uses PyPI package reuse, run "pipx install reuse" to get it
printf "\n\n\nRunning reuse linting\n"
reuse lint || exit_status=$?

# Don't exit 0 if we had errors
exit "${exit_status:-0}"
